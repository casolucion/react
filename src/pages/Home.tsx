import React from "react";

const Home: React.FC = () => {
  return (
    <div
      className="Home d-flex justify-content-center align-items-center vh-100"
      data-testid="HomeComponent"
    >
      <h1>Titulo</h1>
      <p>
        Descripcion: Rick y Morty es una serie de televisión estadounidense de
        animación para adultos creada por Justin Roiland y Dan Harmon para Adult
        Swim. La serie sigue las desventuras de un científico, Rick, y su
        fácilmente influenciable nieto, Morty, quienes pasan el tiempo entre la
        vida doméstica y los viajes espaciales, temporales e intergalácticos.
        Roiland es el encargado de darle voz a los dos personajes principales,
        la serie también incluye las voces de Chris Parnell, Spencer Grammer, y
        Sarah Chalke.
      </p>
      <button
        type="button"
        className="btn btn-lg btn-primary"
        data-testid="ButtonSubmit"
      >
        React Course Base
      </button>
      <div />
    </div>
  );
};

export default Home;
